import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My App';
  name = 'Sushant Sahu';
  server_status = 'No server is running at the moment';
  server_name = 'No Server';
  allow_sever = false;
  server_created = false;

  constructor() {

    setTimeout(() => {
      this.allow_sever = true;
    }, 2000);

  }


  serverClicked() {
    this.server_created = true;
    this.server_status = 'server is running @ 2400 ' + this.server_name;
  }


  updateServer(e: Event) {
    // console.log(e);
    this.server_name = (<HTMLInputElement>e.target).value;
  }

  getColor() {
    return (this.server_created) ? 'green' : 'red';
  }



}
